package com.tobojek.app.service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import com.tobojek.app.model.FirebaseToken;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by David Studio on 10/13/2017.
 */

public class GoridemeInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        saveToken(FirebaseInstanceId.getInstance().getToken());
    }

    private void saveToken(String tokenId) {
        FirebaseToken token = new FirebaseToken(tokenId);
        EventBus.getDefault().postSticky(token);
    }

}
