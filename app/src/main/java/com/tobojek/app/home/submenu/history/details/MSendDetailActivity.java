package com.tobojek.app.home.submenu.history.details;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.tobojek.app.GoridemeAplication;
import com.tobojek.app.api.ServiceGenerator;
import com.tobojek.app.api.service.BookService;
import com.tobojek.app.model.DetailTransaksiMSend;
import com.tobojek.app.model.User;
import com.tobojek.app.model.json.book.detailTransaksi.GetDataTransaksiMSendResponse;
import com.tobojek.app.model.json.book.detailTransaksi.GetDataTransaksiRequest;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by David Studio on 24/02/2017.
 */

public class MSendDetailActivity extends AppCompatActivity {

    public static final String ID_TRANSAKSI = "IDTransaksi";

    @BindView(com.tobojek.app.R.id.mSendDetail_title)
    TextView title;

    @BindView(com.tobojek.app.R.id.mSendDetail_tanggal)
    TextView tanggalPengiriman;
    @BindView(com.tobojek.app.R.id.mSendDetail_alamatAsal)
    TextView alamatAsal;
    @BindView(com.tobojek.app.R.id.mSendDetail_alamatTujuan)
    TextView alamatTujuan;
    @BindView(com.tobojek.app.R.id.mSendDetail_namaPenerima)
    TextView namaPenerima;
    @BindView(com.tobojek.app.R.id.mSendDetail_nomorPenerima)
    TextView nomorPenerima;
    @BindView(com.tobojek.app.R.id.mSendDetail_namaBarang)
    TextView namaBarang;
    @BindView(com.tobojek.app.R.id.total_cost)
    TextView totalCost;

    private String idTransaksi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.tobojek.app.R.layout.activity_msend_detail);
        ButterKnife.bind(this);

        title.setText("Detail m-Send");
        idTransaksi = getIntent().getStringExtra(ID_TRANSAKSI);
        Realm realm = GoridemeAplication.getInstance(this).getRealmInstance();
        User loginUser = realm.copyFromRealm(GoridemeAplication.getInstance(this).getLoginUser());
        BookService service = ServiceGenerator.createService(BookService.class, loginUser.getEmail(), loginUser.getPassword());

        GetDataTransaksiRequest param = new GetDataTransaksiRequest();
        param.setIdTransaksi(idTransaksi);
        service.getDataTransaksiMSend(param).enqueue(new Callback<GetDataTransaksiMSendResponse>() {
            @Override
            public void onResponse(Call<GetDataTransaksiMSendResponse> call, Response<GetDataTransaksiMSendResponse> response) {
                if(response.isSuccessful()) {
                    if(response.body().getDataTransaksi().isEmpty()) {
                        onFailure(call, new Exception());
                    } else {
                        DetailTransaksiMSend detail = response.body().getDataTransaksi().get(0);
                        updateUI(detail);
                    }
                } else {
                    onFailure(call, new Exception());
                }
            }

            @Override
            public void onFailure(Call<GetDataTransaksiMSendResponse> call, Throwable t) {
                Toast.makeText(MSendDetailActivity.this, "Silahkan coba lagi lain waktu.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateUI(DetailTransaksiMSend detail) {
        SimpleDateFormat sdfTo = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.US);

        String formattedDate = sdfTo.format(detail.getWaktuOrder());
        tanggalPengiriman.setText(formattedDate);
        alamatAsal.setText(detail.getAlamatAsal());
        alamatTujuan.setText(detail.getAlamatTujuan());
        namaPenerima.setText(detail.getNamaPenerima());
        nomorPenerima.setText(detail.getTeleponPenerima());
        namaBarang.setText(detail.getNamaBarang());

        String totalcostt = String.format(Locale.US, "$ %s",
                NumberFormat.getNumberInstance(Locale.US).format(detail.getHarga()));
        totalCost.setText(totalcostt);
    }
}
