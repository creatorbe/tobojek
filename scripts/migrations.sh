#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Usage: migrate_android_x <root path of your project>"
    exit
fi

project_root_dir="$1"
echo "Migrating to AndroidX in '$project_root_dir'..."

kernel_name=`uname -s`
IS_MAC_OS=false
if [ $kernel_name = "Darwin" ]; then
    IS_MAC_OS=true
fi

if $IS_MAC_OS; then
    sed_cmd="gsed -i"
else
    sed_cmd="sed -i"
fi

xml_files=`find $project_root_dir -not -path "*/.repo/*" -not -path "*/.git/*" -not -path "*/build/*" -not -path "*/out/*" -not -path "*/.idea/*" -type f -name "*.xml"`

echo $xml_files | xargs grep -l android.support.design.widget.AppBarLayout | xargs $sed_cmd "s/android.support.design.widget.AppBarLayout/com.google.android.material.appbar.AppBarLayout/g"

echo $xml_files | xargs grep -l android.support.design.widget.TabLayout | xargs $sed_cmd "s/android.support.design.widget.TabLayout/com.google.android.material.tabs.TabLayout/g"

echo $xml_files | xargs grep -l androidx.constraintlayout.Guideline | xargs $sed_cmd "s/androidx.constraintlayout.Guideline/androidx.constraintlayout.widget.Guideline/g"

echo $xml_files | xargs grep -l androidx.constraintlayout.ConstraintLayout | xargs $sed_cmd "s/androidx.constraintlayout.ConstraintLayout/androidx.constraintlayout.widget.ConstraintLayout/g"

echo $xml_files | xargs grep -l androidx.appcompat.widget.CardView | xargs $sed_cmd "s/androidx.appcompat.widget.CardView/androidx.cardview.widget.CardView/g"

echo $xml_files | xargs grep -l android.support.design.widget.CollapsingToolbarLayout | xargs $sed_cmd "s/android.support.design.widget.CollapsingToolbarLayout/com.google.android.material.appbar.CollapsingToolbarLayout/g"

echo $xml_files | xargs grep -l android.support.v7.widget.Toolbar | xargs $sed_cmd "s/android.support.v7.widget.Toolbar/androidx.appcompat.widget.Toolbar/g"

echo $xml_files | xargs grep -l androidx.core.view.ViewPager | xargs $sed_cmd "s/androidx.core.view.ViewPager/androidx.viewpager.widget.ViewPager/g"

echo $xml_files | xargs grep -l androidx.appcompat.widget.RecyclerView | xargs $sed_cmd "s/androidx.appcompat.widget.RecyclerView/androidx.recyclerview.widget.RecyclerView/g"

echo $xml_files | xargs grep -l android.support.design.widget.FloatingActionButton | xargs $sed_cmd "s/android.support.design.widget.FloatingActionButton/com.google.android.material.floatingactionbutton.FloatingActionButton/g"

echo $xml_files | xargs grep -l android.support.design.widget.TextInputLayout | xargs $sed_cmd "s/android.support.design.widget.TextInputLayout/com.google.android.material.textfield.TextInputLayout/g"

echo $xml_files | xargs grep -l android.support.design.widget.TextInputEditText | xargs $sed_cmd "s/android.support.design.widget.TextInputEditText/com.google.android.material.textfield.TextInputEditText/g"

echo $xml_files | xargs grep -l android.support.design.widget.BottomNavigationView | xargs $sed_cmd "s/android.support.design.widget.BottomNavigationView/com.google.android.material.bottomnavigation.BottomNavigationView/g"

echo $xml_files | xargs grep -l android.support.wear.widget.CircularProgressLayout | xargs $sed_cmd "s/android.support.wear.widget.CircularProgressLayout/androidx.wear.widget.CircularProgressLayout/g"
